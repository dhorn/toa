package com.theotherapocalypse.units;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import com.theotherapocalypse.essential.ToABasicUnit;
import com.theotherapocalypse.essential.helper.ToAErrorLogger;
import com.theotherapocalypse.essential.helper.ToAUnits;

public class ToATank extends ToABasicUnit
{
	private Animation up;
	private Animation down;
	private Animation left;
	private Animation right;

	public ToATank(float x, float y)
	{
		super(x, y);

		try
		{
			Image[] movementUp =
			{ new Image("assets/sprites/tmp_tank/tmp_tank_moveUp1.png"), new Image("assets/sprites/tmp_tank/tmp_tank_moveUp2.png") };
			Image[] movementDown =
			{ new Image("assets/sprites/tmp_tank/tmp_tank_moveDown1.png"), new Image("assets/sprites/tmp_tank/tmp_tank_moveDown2.png") };
			Image[] movementLeft =
			{ new Image("assets/sprites/tmp_tank/tmp_tank_moveLeft1.png"), new Image("assets/sprites/tmp_tank/tmp_tank_moveLeft2.png") };
			Image[] movementRight =
			{ new Image("assets/sprites/tmp_tank/tmp_tank_moveRight1.png"), new Image("assets/sprites/tmp_tank/tmp_tank_moveRight2.png") };

			int[] duration =
			{ 300, 300 };

			this.up = new Animation(movementUp, duration, false);
			this.down = new Animation(movementDown, duration, false);
			this.left = new Animation(movementLeft, duration, false);
			this.right = new Animation(movementRight, duration, false);
			this.sprite = right;
		} catch (SlickException e)
		{
			ToAErrorLogger.log(e.getLocalizedMessage() + "\n\t\t" + e.getStackTrace());
			e.printStackTrace();
		}
	}

	public String getUnitType()
	{
		return ToAUnits.UNIT_TANK;
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		this.speed = new Vector2f(0, 0);

//		Input input = container.getInput();
//
//		if (input.isKeyDown(Input.KEY_UP))
//		{
//			sprite = up;
//			this.speed.y = -1 * 0.1f;
//		} else if (input.isKeyDown(Input.KEY_DOWN))
//		{
//			sprite = down;
//			this.speed.y = +1 * 0.1f;
//		} else if (input.isKeyDown(Input.KEY_LEFT))
//		{
//			sprite = left;
//			this.speed.x = -1 * 0.1f;
//		} else if (input.isKeyDown(Input.KEY_RIGHT))
//		{
//			sprite = right;
//			this.speed.x = +1 * 0.1f;
//		}
//	
		this.sprite.update(delta);
	
		super.update(container, delta);
//		System.out.println("Update: " + position.x + " " + position.y + "; " + speed.x + " " + speed.y);
	}

}
