package com.theotherapocalypse.essential;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.pathfinding.Mover;

import com.theotherapocalypse.essential.helper.ToAUnits;

public class ToABasicUnit implements Mover
{
	protected Vector2f position; // Unit's position
	protected Vector2f speed; // Unit*s speed (position change over time delta)
	protected Animation sprite; // sprite that is rendered at this entity's place every frame
	protected int movementPoints; // movementPoints a unit has. They are reset each turn

	public ToABasicUnit(float x, float y)
	{
		this.speed = new Vector2f(0, 0);
		this.position = new Vector2f(x, y);
		this.sprite = null;
		this.movementPoints = 0;
	}

	public void render(GameContainer container, Graphics g) throws SlickException
	{
		// render sprite at position "position"
		sprite.draw(this.position.x, this.position.y);
	}

	public void update(GameContainer container, int delta) throws SlickException
	{
		// update animations and stuff
		this.position.x += this.speed.x * delta;
		this.position.y += this.speed.y * delta;
		sprite.update(delta);
	}

	public void beginTurn()
	{
		this.movementPoints = ToAUnits.getMovementPointsForUnit(this.getUnitType());
	}

	public boolean getHasMovementLeft()
	{
		return movementPoints > 0;
	}

	public String getUnitType()
	{
		return "Error. No Unit type";
	}

	public Vector2f getPosition()
	{
		return this.position;
	}

	public void setPosition(Vector2f position)
	{
		this.position = position;
	}

	public void setSprite(Animation sprite)
	{
		this.sprite = sprite;
	}
}
