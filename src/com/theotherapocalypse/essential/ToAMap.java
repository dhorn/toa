package com.theotherapocalypse.essential;

import java.util.Hashtable;

import org.newdawn.slick.*;
import org.newdawn.slick.tiled.*;
import org.newdawn.slick.util.pathfinding.*;

import com.theotherapocalypse.essential.helper.ToAIntVector2D;
import com.theotherapocalypse.essential.helper.ToATiles;

public class ToAMap implements TileBasedMap
{
	private TiledMap map;
	private Hashtable<ToAIntVector2D, String> tileTypeArray;
		
	public ToAMap(String tmxResourcePath) throws SlickException
	{
		this.map = new TiledMap(tmxResourcePath);
		this.tileTypeArray = new Hashtable<ToAIntVector2D, String>();
		
		for (int y = 0; y < this.map.getHeight(); y++)
		{
			for (int x = 0; x < this.map.getWidth(); x++)
			{
				this.tileTypeArray.put(new ToAIntVector2D(x, y), this.map.getTileProperty(this.map.getTileId(x, y, 0), "tiletype", "water"));
			}
		}
	}
	
	public void render(GameContainer container, Graphics g) throws SlickException
	{
		this.map.render(0, 0);
	}
	
	/* TileBasedMap */
	public boolean blocked(PathFindingContext context, int tx, int ty)
	{
		boolean isTileBlocked = false;
		
		if (ToATiles.getMovementCostForUnitForTile(((ToABasicUnit)context.getMover()).getUnitType(), this.tileTypeArray.get(new ToAIntVector2D(tx, ty))) == -1)
			isTileBlocked = true;
		
		return isTileBlocked;
	}

	public float getCost(PathFindingContext context, int tx, int ty)
	{
		return ToATiles.getMovementCostForUnitForTile(((ToABasicUnit)context.getMover()).getUnitType(), this.tileTypeArray.get(new ToAIntVector2D(tx, ty)));
	}

	public int getHeightInTiles()
	{
		return this.map.getHeight();
	}

	public int getWidthInTiles()
	{
		return this.map.getWidth();
	}

	public void pathFinderVisited(int x, int y)
	{
		// Probably nothing to do here
	}
}
