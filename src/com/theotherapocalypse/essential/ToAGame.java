package com.theotherapocalypse.essential;

import java.util.ArrayList;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;

import com.theotherapocalypse.essential.helper.ToAErrorLogger;
import com.theotherapocalypse.essential.helper.ToAGameStates;
import com.theotherapocalypse.units.ToATank;

/**
 * 
 * @author Dominik
 * 
 */
public class ToAGame extends BasicGame
{
	public static final int SCREEN_WIDTH = 1280;
	public static final int SCREEN_HEIGHT = 720;

	private ArrayList<ToABasicUnit> playerUnits;
	private ToAMap map;
	private ToABasicUnit selectedUnit;
	private ToAGameStates gamestate;
	private Vector2f viewPosition;

	public ToAGame()
	{
		super("The other Apocalypse game");

		this.playerUnits = new ArrayList<ToABasicUnit>();
		this.selectedUnit = null;
		this.gamestate = ToAGameStates.GAMESTATE_PLAYER_TURN_INIT;
		this.viewPosition = new Vector2f(0, 0);
	}

	public static void main(String[] arguments)
	{
		try
		{
			AppGameContainer app = new AppGameContainer(new ToAGame());
			app.setDisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT, false);
			app.start();
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void init(GameContainer container) throws SlickException
	{
		this.map = new ToAMap("assets/tiled_maps/map_tmp/map_tmp_tilemap.tmx");
		this.playerUnits.add(new ToATank(0, 0));
		
		container.getGraphics().setBackground(new Color(0.1f, 0.1f, 0.1f, 1.0f));
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		// Determine current game state
		switch (this.gamestate)
		{
		case GAMESTATE_PLAYER_TURN_INIT:
			beginPlayerTurn();
			this.gamestate = ToAGameStates.GAMESTATE_PLAYER_TURN_INACTION;
			break;
		case GAMESTATE_PLAYER_TURN_INACTION:
			doPlayerTurn(container, delta);
			break;
		case GAMESTATE_PLAYER_TURN_ENDTURN:

			break;

		default:
			ToAErrorLogger.log("ERROR: Unknown Gamestate");
			break;
		}
	}

	public void render(GameContainer container, Graphics g) throws SlickException
	{

		if (this.selectedUnit != null)
			g.translate(-viewPosition.x, -viewPosition.y);
		
		this.map.render(container, g);

		for (ToABasicUnit unit : this.playerUnits)
		{
			unit.render(container, g);
		}

		g.resetTransform();
	}

	private void beginPlayerTurn()
	{
		for (ToABasicUnit unit : this.playerUnits)
		{
			unit.beginTurn();
		}
		
		this.selectedUnit = this.playerUnits.get(0);
	}
	
	private void doPlayerTurn(GameContainer container, int delta) throws SlickException
	{
		for (ToABasicUnit unit : this.playerUnits)
		{
			unit.update(container, delta);
		}
	}
	
//	private void setViewPosition()
}