package com.theotherapocalypse.essential.helper;

import java.util.Hashtable;

public class ToATiles {
	/* define all tiles as string constants */
	public static final String TILE_GRASS = "grass";
	public static final String TILE_WATER = "water";
	
	/* helper method */
	public static boolean TileEqualsTile(String tile1, String tile2)
	{
		return tile1.equals(tile2);
	}

	
	private static ToATiles instance = null;
	private Hashtable<String, Hashtable<String, Integer>> tileCosts;
	
	/**
	 * Default constructor, which is not visible from outside of this class
	 */
	private ToATiles() 
	{
		this.tileCosts = new Hashtable<String, Hashtable<String, Integer>>();
		
		/* tileCost for each tile for each unit */
		
		// Movement costs for tanks
		Hashtable<String, Integer> tankMovementCosts = new Hashtable<String, Integer>();
		tankMovementCosts.put(ToATiles.TILE_WATER, -1);	// tanks can't pass water
		tankMovementCosts.put(ToATiles.TILE_GRASS, 1);	// tanks can pass grass
		
		/* add to tileCosts hashtable */
		this.tileCosts.put(ToAUnits.UNIT_TANK, tankMovementCosts);
	}

	public static int getMovementCostForUnitForTile(String unit, String tile)
	{
		return ToATiles.getInstance().getMovementCostForUnitOnTile(unit, tile);
	}
	
	private int getMovementCostForUnitOnTile(String unit, String tile)
	{
		int movementCost = -1;
		
		Hashtable<String, Integer> costsForUnit = this.tileCosts.get(unit);
		if (costsForUnit != null)
		{
			if (costsForUnit.get(tile) != null)
			{
				movementCost = costsForUnit.get(tile);
			} 
			else
			{
				ToAErrorLogger.log("Could not find movement cost for tile \"" + tile + "\" for unit \"" + unit + "\"  in ToATileCost. Defaulting to \"-1\"");
			}
		}
		else
		{
			ToAErrorLogger.log("Could not find unit \"" + unit + "\" in ToATileCost. Defaulting movement cost to \"-1\"");
		}
		
		return movementCost;
	}
	
	/**
	 * Static method, returns only instance of this class
	 */
	private static ToATiles getInstance()
	{
		if (instance == null)
		{
			instance = new ToATiles();
		}
		
		return instance;
	}
}
