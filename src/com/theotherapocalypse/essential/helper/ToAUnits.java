package com.theotherapocalypse.essential.helper;

import java.util.Hashtable;

public class ToAUnits
{
	public static final String UNIT_TANK = "tank";

	private Hashtable<String, Integer> movementPoints = new Hashtable<String, Integer>();
	
	
	public static boolean unitEqualsUnit(String unit1, String unit2)
	{
		return unit1.equals(unit2);
	}
	
	public static int getMovementPointsForUnit(String unit)
	{		
		return getInstance().getMovementPoints(unit);
	}
	
	private int getMovementPoints(String unit)
	{
		int movementPoints = 0;
		
		if (this.movementPoints.contains(unit))
		{
			movementPoints = this.movementPoints.get(unit);
		}
		
		return movementPoints;
	}
	
	// Singleton
	private static ToAUnits instance;
	private static ToAUnits getInstance()
	{
		if (instance == null)
		{
			instance = new ToAUnits();
		}
		
		return instance;
	}
	
	private ToAUnits() // prevent instantiation
	{
		this.movementPoints.put(UNIT_TANK, 5);
	}
}
