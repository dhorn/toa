package com.theotherapocalypse.essential.helper;

public enum ToAGameStates
{
	GAMESTATE_PLAYER_TURN_INIT,
	GAMESTATE_PLAYER_TURN_INACTION,
	GAMESTATE_PLAYER_TURN_ENDTURN;
}
