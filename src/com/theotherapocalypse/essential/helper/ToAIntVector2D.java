package com.theotherapocalypse.essential.helper;

public class ToAIntVector2D
{
	private int x;
	private int y;
	
	public ToAIntVector2D(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(ToAIntVector2D vec)
	{
		if (this.getX() == vec.getX() && this.getY() == vec.getY())
		{
			return true;
		}
		
		return false;
	}

	public int getX()
	{
		return this.x;
	}
	
	public void setX(int x)
	{
		this.x = x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public void setY(int y)
	{
		this.y = y;
	}
}
