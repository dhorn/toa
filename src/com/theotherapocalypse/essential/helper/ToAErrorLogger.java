package com.theotherapocalypse.essential.helper;

public class ToAErrorLogger
{
	private static ToAErrorLogger instance = null;

	private boolean debugLogEnabled = true;
	
	private ToAErrorLogger()
	{
		
	}
	
	private static ToAErrorLogger getInstance()
	{
		if (instance == null)
		{
			instance = new ToAErrorLogger();
		}
		
		return instance;
	}
	
	public static void log(String error)
	{
		ToAErrorLogger.getInstance().logError(error);
	}
	
	public static void enableDebugMode()
	{
		ToAErrorLogger.getInstance().enableDebugLogging(true);
	}
	
	public static void disableDebugMode()
	{
		ToAErrorLogger.getInstance().enableDebugLogging(false);
	}
	
	/**
	 * log error to log file. If debug mode is enabled also log to console
	 * @param errorString
	 * error string to write to log output
	 */
	private void logError(String errorString)
	{
		if (debugLogEnabled)
		{
			System.err.println("ERROR: " + errorString);
		}
		
		// TODO: log to file
	}
	
	/**
	 * enable or disable error logging to console
	 * @param enable 
	 * boolean value that specifies whether logging to console should be enabled or not
	 */
	private void enableDebugLogging(boolean enable)
	{
		this.debugLogEnabled = enable;
	}
}
